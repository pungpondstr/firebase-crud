import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home')
  },
  {
    path: '/create',
    name: 'Create',
    component: () => import('../views/Create')
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: () => import('../views/Edit')
  },
  {
    path: '/show/:id',
    name: 'Show',
    component: () => import('../views/Show')
  },
]

const router = new VueRouter({
  routes
})

export default router
