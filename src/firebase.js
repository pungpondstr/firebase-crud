import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDID_WHuEFW4SCQjeJSrLDZhKdznwIbgt4",
    authDomain: "crud-blog-fa7c7.firebaseapp.com",
    projectId: "crud-blog-fa7c7",
    storageBucket: "crud-blog-fa7c7.appspot.com",
    messagingSenderId: "31710226752",
    appId: "1:31710226752:web:df7bccde8cb73c351ef2a5"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;